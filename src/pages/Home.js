import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for Everyone, Everywhere!",
		destination: "/courses",
		label: "Enroll Now!"
	}


export default function Home() {
	
	return (
		<>
			<Banner data={data}/>
	  		<Highlights/>
		</>
	)
}