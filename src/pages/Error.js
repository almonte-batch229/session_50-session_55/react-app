import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title: "Page Not Found",
		content: "The page you're looking for cannot be found.",
		destination: "/",
		label: "Go Back to Homepage"
	}

	return(
		<>
			<Banner data={data}/>
		</>
	)
}