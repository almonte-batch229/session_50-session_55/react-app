import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProps}) {

	// checks to see if the data was succesfully passed.
	// console.log(courseProps);
	// console.log(typeof courseProps);

	// destructure the data to avoid using the dot notation 
	const {name, description, price, _id} = courseProps;

	/*
		3 Hooks in React
			1. useState
			2. useEffect
			3. useContext 
	*/

	// Use the useState hook for the component to be able to store state
	// States are used to 
	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue);
	*/

	const [count, setCount] = useState(0);
	const [seat, seatCount] = useState(30);
	// console.log(useState(0))

	/*
		function enroll() {

			if (seat == 0) {
				alert("No more seats available!")
			}
			else {
			setCount(count+1)
			seatCount(seat-1)
			// console.log("Enrollees " + count)
			}
		};
	*/

	return (
        // Activity Solution
        <Card>
	        <Card.Body className="my-3">
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            {/*<Card.Subtitle>Enrollees:</Card.Subtitle>*/}
	            {/*<Card.Text>{count} Enrollees</Card.Text>*/}
	            {/*<Button onClick={enroll} variant="primary">Enroll</Button>*/}
	        	<Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
	        </Card.Body>
	    </Card>

	    // Activity Answer: 
		/*  <Card className="cardCourseCard p-3 m-4">
		        <Card.Body>
		            <Card.Title>
		                Sample Course
		            </Card.Title>
		            <Card.Text>
		                Description:<br/>
		                This is a sample course offering.<br/><br/>
		                Price:<br/>
		                Php 40,000
		            </Card.Text>
		            <Button variant="primary">Enroll</Button>
		        </Card.Body>
		    </Card>
		*/
	)
}

